![Logo WebEngineering](http://www.webengineering.fr/webengineering/www/webengineering/images/webengineering-logo.png)

# CHALLENGE WEBENGINEERING - STEP 2 - BUILD A WEB APP

In this exercice we want to evaluate you capacity to create a javascript WebApp. 
We don't want you to use an unfriendly framework, so you only have to respect the following rules : 

* Use Javascript
* Be creative
* Be responsive
* Don't access directly to the data but use the api you build in step-1 
* The WebApp has to be able to create Likes and Comments, but developpers and/or code library can be in read access only.

The application could be an admin panel or not...

# BONUS # 

If you have enough time and only if you want to, you can implement the following functionalities in your WebApp.

* Provide a full CRUD for developpers and code librairies.
* Provide a search engine to find a code by Languages, Authors or name.

# Informations #
WebEngineering stack: 

* MeteorJs
* ReactJs
