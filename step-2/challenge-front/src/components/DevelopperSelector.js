import React from 'react';

import DevelopperItem from './DevelopperItem';

const DevelopperSelector = ({ developpers, selectedDevelopperId, developperSelectHandler }) => {
  const displayDeveloppers = () => 
    developpers.map((developper) => 
      <DevelopperItem 
        key={developper.id} 
        developper={developper}
        selectedDevelopperId={selectedDevelopperId}
        developperSelectHandler={developperSelectHandler}
      />);

  return (
      <nav className="col-md-2 sidebar">
          <h6 className="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">Sélectionnez un développeur</h6>
          <ul className="nav flex-column">
              { displayDeveloppers() }
          </ul>
      </nav>
  );
}

export default DevelopperSelector;