import React, {useState, useEffect} from 'react';
import axios from 'axios';

import CodeItem from './CodeItem';

const CodeDisplayer = ({selectedDevelopperId}) => {
  const [codes, setCodes] = useState([]);
  // get the codes linked to this developper id
  // only run on 'selectedDevelopperId' change
  useEffect(() => {
    axios
      .get('http://localhost:3001/codes?dev_id=' + selectedDevelopperId)
      .then(response => {
        setCodes(response.data)
      })
  }, [selectedDevelopperId]);

  let display;
  if(selectedDevelopperId === 0) {
    display = <h3>Vous n'avez pas sélectionné de développeur :(</h3>
  } else if (codes.length === 0) { 
    display = <h3>Ce développeur n'a aucun code pour le moment</h3>
  } else {
    display = codes.map((code) => <CodeItem key={code.id} code={code} />);
  }
  return (
    <main className="col-md-8 offset-md-1 py-4">
      {display}
    </main>
  );
}

export default CodeDisplayer;