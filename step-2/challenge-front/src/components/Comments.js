import React from 'react';

const Comments = ({comments}) => {  
  const displayComments = () => comments.map(comment => <p key={comment.id} className="card-text">{comment.text}</p>)

  return (
    <>
      { displayComments() }
    </>
  );
}

export default Comments;