import React, {useState, useEffect} from 'react';
import axios from 'axios';

import NewCommentForm from './NewCommentForm';
import Comments from './Comments';

const CodeItem = ({code}) => {
  const [comments, setComments] = useState([]);
  const [newComment, setNewComment] = useState('');

  // get the comments linked to this code id
  useEffect(() => {
    axios
      .get('http://localhost:3001/comments?code_id=' + code.id)
      .then(response => {
        setComments(response.data);
      });
    }, [code.id]);
  
  const createNewComment = () => {
    const newCommentObject = {code_id: code.id, text: newComment}
    axios
      .post('http://localhost:3001/comments', newCommentObject)
      .then(response => {
        setComments(comments.concat(response.data));
        setNewComment('');
      });
  }
  const handleCommentChange = (event) => setNewComment(event.target.value);

  return (
    <div className="card mb-3">
      <div className="card-body">
        <h5 className="card-title">
          {code.name} <span className="badge badge-pill badge-primary">{code.language}</span>
          <span className="badge badge-pill badge-warning">v{code.version}</span>
        </h5>
        <Comments comments={comments} />
        <NewCommentForm newComment={newComment} commentChangeHandler={handleCommentChange} newCommentHandler={createNewComment} />
      </div>
    </div>
  );
}

export default CodeItem;