import React from 'react';

const DevelopperItem = ({ developper, selectedDevelopperId, developperSelectHandler }) => {
  return (
    <li className="nav-item">
      <button 
        type="button" 
        onClick={developperSelectHandler(developper.id)}
        className={"btn btn-link nav-link" + (selectedDevelopperId === developper.id ? ' active' : '')}>
          {developper.fullname}
        </button>
    </li>
  );
}
  
export default DevelopperItem;