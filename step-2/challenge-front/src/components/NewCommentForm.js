import React from 'react';

const NewCommentForm = ({newComment, commentChangeHandler, newCommentHandler}) => {
  return (
    <form>
      <div className="input-group mb-3">
        <input
          value={newComment}
          onChange={commentChangeHandler}
          type="text"
          className="form-control"
          placeholder="Commentaire"/>
        <div className="input-group-append">
            <button className="btn btn-outline-primary" type="button" onClick={newCommentHandler}>Envoyer</button>
        </div>
      </div>
    </form>
  );
}

export default NewCommentForm;