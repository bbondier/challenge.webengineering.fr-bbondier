import React, {useState, useEffect} from 'react';
import axios from 'axios';

import DevelopperSelector from './components/DevelopperSelector';
import CodeDisplayer from './components/CodeDisplayer';

const App = () => {
  const [developpers, setDeveloppers] = useState([]);
  const [selectedDevelopperId, setSelectedDevelopperId] = useState(0);

  // get the list of developper from the API
  // second argument is an empty array to only run on first component render
  useEffect(() => {
    axios
      .get('http://localhost:3001/developpers')
      .then(response => {
        setDeveloppers(response.data);
      })
  }, []);

  const handleDevelopperSelect = (id) => () => setSelectedDevelopperId(id);

  return (
    <>
      <nav className="navbar navbar-dark">
        <h2 className="navbar-brand mx-auto">Challenge WebEngineering</h2>
      </nav>
      <div className="row">
        <DevelopperSelector 
          developpers={developpers}
          selectedDevelopperId={selectedDevelopperId}
          developperSelectHandler={handleDevelopperSelect} />
        <CodeDisplayer selectedDevelopperId={selectedDevelopperId} />
      </div>
    </>
  );
}

export default App;
