## Challenge Webengineering

Les fichiers de l'application React se trouvent dans `step-2/challenge-front`

Dans ce répertoire, exécuter un `npm install` pour récupérer les dépendances.

Une fois l'installation terminée :

* `npx json-server --watch db.json --port 3001` pour lancer l'API mock
* `npm start` dans un autre terminal pour lancer l'environnement de développement
* L'application est accessible sur `http://localhost:3000`